# kbvis

Visualize keyboard stroke frequency.

## Usage

* https://condescending-brattain-ed7bf3.netlify.com/


## Authors

* **Akihito Morita**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.

